from WMCore.Configuration import Configuration
config = Configuration()

config.section_("General")
config.General.requestName = "privateMCProduction_#WHOAMI#_#BASENAME#_stepLHE-FGEN-SIM_#REQUESTDATE#"
config.General.workArea = 'crab_privateMCProduction'
config.General.transferLogs = True

config.section_("JobType")
config.JobType.pluginName = 'PrivateMC'
config.JobType.psetName = 'customLHE-FGEN-SIM_cfg.py'
config.JobType.maxJobRuntimeMin = 2750
config.JobType.maxMemoryMB = 4000
config.JobType.inputFiles = ['GeneratorInterface/LHEInterface/data/run_generic_tarball_xrootd.sh']
config.JobType.disableAutomaticOutputCollection = False

config.section_("Data")
config.Data.outputPrimaryDataset = 'privateMCProductionLHEGEN'
config.Data.splitting = 'EventBased'
#config.Data.unitsPerJob = 30000
config.Data.unitsPerJob = 25000
config.Data.totalUnits = #NUMBEREVENTS#
config.Data.publication = True
config.Data.outputDatasetTag = 'eventLHE-FGEN-SIM_#BASENAME#_#NUMBEREVENTS#evts_#REQUESTDATE#'
config.Data.outLFNDirBase = '/store/user/acrobert/mcprod/#BASENAME#/'

config.section_("Site")
config.Site.storageSite = 'T3_US_CMU'
#config.Site.whitelist = ['T2_*']

config.section_("User")
## only german users
#config.User.voGroup = "dcms"

