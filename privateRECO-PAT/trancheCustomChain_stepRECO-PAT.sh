#!/bin/bash

# Define workdir
export WORKDIR=`pwd`

# Define location of GenSim samples, warning make sure that you run only one time on the same folder since otherwise we will produce two times the events.
# You will get an error message if you try to reuse some of the input files, so please make sure that you start this production only after all GenSim events are produced.
# Furthermore, you have to give an absolute path name
#export GENSIMLOC=/privateMCProductionLHEGEN/mharrend-eventLHEGEN-TTToSemiLepton_hvq_ttHtranche3-962bade98c5fada66831bc83bbc241c7/USER
export GENSIMLOC=/privateMCProductionLHEGEN/acrobert-eventHLT_ZZTo2L2B-NLO-1j-g0_656779evts_2023103019101698692205-b403a189a2d057e62e59ed092120c7f4/USER

# Use crab for grid submitting, adjust crabconfig.py accordingly beforehand
# Use of crab is necessary so far
export USECRAB="True"

######### Do not change anything behind this line ###############


export STARTDIR=`pwd`
echo "Start dir was:"
echo $STARTDIR

echo "Workdir set is:"
echo $WORKDIR
mkdir -p $WORKDIR
echo "Created workdir"
cd $WORKDIR
echo "Changed into workdir"

echo "Install CMSSW in workdir"
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
scram project CMSSW_10_6_17_patch1
cd CMSSW_10_6_17_patch1/src
eval `scramv1 runtime -sh`
echo "Loaded CMSSW_10_6_17_patch1"

dasgoclient --query="file dataset=${GENSIMLOC} instance=prod/phys03" > filelist_crab.txt
export NUMBERFILES=$(wc -l < "filelist_crab.txt")

dasgoclient --query="dataset=${GENSIMLOC} instance=prod/phys03 | grep dataset.nevents" > dsetnevents_crab.txt
NEVENTS=0
while read line;
do
    if [ "$line" != "" ];
    then
        NEVENTS=$line;
    fi;
done < <(cat dsetnevents_crab.txt)

sed -e "s@#INPUTDATASET#@${GENSIMLOC}@g" $STARTDIR/crabconfig_draft.py > ./crabconfig_dsetInserted.py
cp $STARTDIR/customChain_stepRECO-PAT_cfg.py .

if [ $USECRAB = "True" ]; then
	echo "Will use crab submission, adjust crabconfig.py accordingly if problems arise"

        echo "Scram b and start of PAT setup"
	scram b -j 4

	echo "Load crab environment, grid environment should be loaded manually in advance if necessary"
	source /cvmfs/cms.cern.ch/crab3/crab.sh

	echo "Change number of events in crab config to"
	echo $NUMBERFILES
	echo " and copy crabconfig.py to workdir"
	sed -e "s/#NEVENTS#/${NEVENTS}/g" ./crabconfig_dsetInserted.py > ./crabconfig_eventsInserted.py
	sed -e "s/#NUMBERFILES#/${NUMBERFILES}/g" ./crabconfig_eventsInserted.py > ./crabconfig_filesInserted.py
	sed -e "s/#REQUESTDATE#/`date  +'%Y%m%d%H%m%s'`/g" ./crabconfig_filesInserted.py > ./crabconfig_dateInserted.py
	sed -e "s/#WHOAMI#/`whoami`/g" ./crabconfig_dateInserted.py > ./crabconfig_UserInserted.py

	export BASENAMEREPLACEFULL=`echo $GENSIMLOC | grep -o 'eventHLT_[A-Za-z0-9\-]*'`
	export BASENAMEREPLACE=${BASENAMEREPLACEFULL:9}
	echo $BASENAMEREPLACE
	sed -e "s/#BASENAME#/${BASENAMEREPLACE}/g" ./crabconfig_UserInserted.py > ./crabconfig.py


        echo "Scram b and start of PAT production"
        scram b -j 4

	echo "Submit crab jobs"
	crab submit crabconfig.py

	echo "Finished with crab submission, check job status manually"
	echo "In the end you should get MiniAOD files."
else
	echo "Local production using cmsRun is not supported."
	exit -1
fi
