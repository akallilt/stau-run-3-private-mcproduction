#ifndef RecHitAnalyzer_h
#define RecHitAnalyzer_h

//---------------------------------------------                                                                                                                                                          
//                                                                                                                                                                                                       
// Package: PhotonClassifier                                                                                                                                                                             
// Class: RecHitAnalyzer                                                                                                                                                                                 
//                                                                                                                                                                                                       
// Author: Andrew C. Roberts                                                                                                                                                                             
// Started 2022/5/18                                                                                                                                                                                     
// Last Updated 2022/5/18                                                                                                                                                                                
//                                                                                                                                                                                                       
//---------------------------------------------

#include <vector>
#include "TCanvas.h"

#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/JetReco/interface/GenJet.h"

#include "DataFormats/BTauReco/interface/JetTag.h"
#include "DataFormats/BTauReco/interface/CandIPTagInfo.h"

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "TTree.h"

class RecHitAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
  public:
    explicit RecHitAnalyzer(const edm::ParameterSet&);
    ~RecHitAnalyzer();

  private:
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;

    TTree* RHTree;

    unsigned long long eventId_;
    unsigned int runId_;
    unsigned int lumiId_;

};

#endif
